var MenuItem = require("./../models/MenuItem"),
    Item = require("./../models/Item"),

    /* *****************************************************************************************************************
     * MenuItems
     * ****************************************************************************************************************/
    root = new MenuItem("root"),

    voorgerechten = new MenuItem("Voorgerechten", root),
    hoofdgerechten = new MenuItem("Hoofdgerechten", root),
    desserten = new MenuItem("Desserten", root),

    // voorgerechten
    soepen = new MenuItem("Soepen", voorgerechten),
    salades = new MenuItem("Salades", voorgerechten),

    // hoofdgerechten
    vlees = new MenuItem("Vlees", hoofdgerechten),
    vis = new MenuItem("Vis", hoofdgerechten),

    // desserten
    ijs = new MenuItem("Ijs", desserten),
    koffie = new MenuItem("Koffie", desserten),

    // soepen
    haaienvinnen = new MenuItem("Haaienvinnen", soepen),
    kervel = new MenuItem("Kervel", soepen),

    // salades
    caesar = new MenuItem("Caesar", salades),
    rabbit = new MenuItem("Rabbit", salades),

    // vleesgerechten
    biefstuk = new MenuItem("Biefstuk", vlees),
    kip = new MenuItem("Kip", vlees),

    // visgerechten
    zalm = new MenuItem("Zalm", vis),
    scampi = new MenuItem("Scampi's", vis),

    // ijs
    dame = new MenuItem("Dame Blanche", ijs),
    bresilienne = new MenuItem("Coup WK", ijs),

    // koffie
    espresso = new MenuItem("Espresso", koffie),
    perongeluko = new MenuItem("Perongelukko", koffie);

/* *********************************************************************************************************************
 * Items
 * ********************************************************************************************************************/
var items = [
    new Item("Haaienvinnen Soep", "Duuun dun duuun dun dun dun dun dun dun dun BOM BOM dun dun dun dun dun dun doo dedoo doo dedoo dede doo dede doo dededoo", 6, haaienvinnen),
    new Item("Kervel Soep", "Een vochtige soep zonder eigen soundtrack of smaak", 3, kervel),

    new Item("Caesar Salade", "Een verfrissende salade. Komt samen met instructies voor het bricoleren van uw eigen dolk! ", 12, caesar),
    new Item("Rabbit's Delight", "Een doodgewone salade, zij het met happy ending!", 50, rabbit),

    new Item("Filet Pur", "De optie 'Bien Cuit' is enkel beschikbaar voor de kip", 28, biefstuk),
    new Item("Kip aan't Spit","Het is kip. Op een spies. Geroosterd boven een vuur. Wat had U dan verwacht?", 14, kip),

    new Item("Kalme zalm", "Niet te verwarren met de totem. Scouts zijn stom!", 24, zalm),
    new Item("Scampi's", "Als u de kleine garnaal niet lust, raden we af zijn groter broertje te bestellen", 21, scampi),

    new Item("Dame Blanche", "Al het lekker van een Banana Split zonder dat vervelende fruit", 5, dame),
    new Item("Coup WK", "In mindere etablisementen ook wel Coup Braziliaan genoemd", 4, bresilienne),

    new Item("Espresso", "Voor de echte mannen onder ons. Ook al bent U dan een vrouw!", 2, espresso),
    new Item("Per Ongelukko", "U moet het zelf weten als u wil gaan zitten morsen met melk of suiker boven een perfect normale kop zwarte koffie", 3, perongeluko),
]

exports.menuRoot = root;
exports.items = items;