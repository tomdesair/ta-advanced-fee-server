var express = require("express"),
    fs = require("fs"),
    routes = require("./routes"),
    items = require("./routes/items"),
    orders = require("./routes/orders"),
    app = express();

app.configure(function(){
    app.use(express.bodyParser());
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    app.use(app.router);
});

app.get('/', routes.index)

app.get('/items', routes.index);

app.get('/items/:menucategory', items.getMenu);

app.get('/items/:menucategory/:submenucategory', items.getSubmenu)

app.get('/items/:menucategory/:submenucategory/:itemname', items.getItem)

app.get('/orders', orders.list);

app.post('/orders', orders.create);

// boot server
app.listen(1337);
console.log('Listening on port 1337');