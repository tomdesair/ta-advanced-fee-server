var nextItemId = 1;
function Item(name, description, price, parent){
    this.id = nextItemId++;
    this.name = name;
    this.description = description;
    this.price = price;

    parent.underlyingItem = this;
    this.path = parent.path;
}

module.exports = exports = Item;