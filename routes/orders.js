var jsonMapper = require("../utils/JsonMapper"),
    data = require("../data/data"),
    Order = require("../models/Order"),
    orders = [];

exports.list = function(req, res){
    res.send(jsonMapper.mapOrders(orders));
};

exports.create = function(req, res){
    var items = getItemsById(req.body.items);
    if(items.length > 0){
        orders.push(new Order(items));
        res.send(200);
    } else {
        res.send(400);
    }
};

function getItemsById(ids){
    var foundItems = [];

    for(var i = 0; i < ids.length; i++){
        for(var j = 0; j < data.items.length; j++ ){
            if(ids[i] === data.items[j].id){
                foundItems.push(data.items[j]);
            }
        }
    }

    return foundItems;
}

