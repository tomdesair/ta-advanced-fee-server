var data = require("../data/data"),
    jsonMapper = require("../utils/JsonMapper");

exports.getMenu = function(req, res){
    var item = resolveChildByName(data.menuRoot, req.params.menucategory.toLowerCase()),
        result = jsonMapper.mapMenuItem(item);

    res.send(result);
}

exports.getSubmenu = function(req, res){
    var parent = resolveChildByName(data.menuRoot, req.params.menucategory.toLowerCase()),
        item = resolveChildByName(parent, req.params.submenucategory.toLowerCase()),
        result = jsonMapper.mapMenuItem(item);

    res.send(result);
}

exports.getItem = function(req, res){
    var grandParent = resolveChildByName(data.menuRoot, req.params.menucategory.toLowerCase()),
        parent = resolveChildByName(grandParent, req.params.submenucategory.toLowerCase()),
        item = resolveChildByName(parent, req.params.itemname.toLowerCase()),
        result = jsonMapper.mapUnderlyingItem(item);

    res.send(result);
}

function resolveChildByName(parent, childName){
    if(!parent)
        return undefined;

    var children = parent.children;
    for(var i = 0; i < children.length; i++){
        if(children[i].path.indexOf(childName) !== -1){
            return children[i];
            break;
        }
    }
    return undefined;
}